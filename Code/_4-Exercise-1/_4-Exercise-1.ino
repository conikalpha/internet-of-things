  int min_temperature = 26;
  int max_temperature = 28;

void setup() {
  // Bean Serial is at a fixed baud rate. Changing the value in Serial.begin() has no effect.
  Serial.begin();
}

void loop() {
  // Get the current ambient temperature in degrees Celsius with a range of -40 C to 87 C.
  int temperature = Bean.getTemperature();
  
  Serial.print("Temperature: ");
  Serial.print(temperature);
  
  if(temperature >= max_temperature){
    //hot
    Serial.println("\t HOT");
    Bean.setLed(255, 0, 0);
    
  }else if(temperature <= min_temperature){
    //cold
    Serial.println("\t COLD");
    Bean.setLed(0, 0, 255);

    
  }else{
    //good
    Serial.println("\t GOOD");
    Bean.setLed(0, 255, 0);
   
  }
  
  
  Bean.sleep(3000);
}
