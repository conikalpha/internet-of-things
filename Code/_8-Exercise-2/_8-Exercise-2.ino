
int treshold = 100;
AccelerationReading previousAccel;
boolean movement = false;


void setup() {
  //open the Serial port
  Serial.begin();
   // Turn off the Bean's LED
  Bean.setLed(0, 0, 0);  
  // Initial reading  
  previousAccel = Bean.getAcceleration(); 
  
  //Print Initial Message
  Serial.println("Alarme Enabled");
  Serial.print("Treshold setted to level ");
  Serial.println(treshold);

}


void loop() {
   serialListener();
   movementListener();
   alertManager();
}

//this functions reads serial messages
void serialListener(){
  
    // if there's any serial available, read it:
   if (Serial.available() > 0) {
     
      // look for the next valid integer in the incoming serial stream:
      int newTreshold = Serial.read() - '0';

      //verify if is a valid number
      if(newTreshold > 0 && newTreshold < 10){
        //map to real value
        treshold = map(newTreshold, 1, 9, 25, 150);
        
        //print the status message
        Serial.print("Treshold setted to level ");
        Serial.print(newTreshold);
        Serial.print(" -> " );
        Serial.println(treshold);
      }else{
        
         //print the status message
         Serial.println("Invalid Treshold, 1 - 9"); 
      }
   }
}


void movementListener(){
  // Get the current acceleration with a conversion of 3.91×10-3 g/unit.
  AccelerationReading currentAccel = Bean.getAcceleration();   
  
  // Find the difference between the current acceleration and that of 200ms ago.
  int accelDifference = getAccelDifference(previousAccel, currentAccel); 
  // Update previousAccel for the next loop.   
  previousAccel = currentAccel;                                            
  
  // Check if the Bean has been moved beyond our threshold.
  if(accelDifference > treshold){   
    movement = true;
  }else{
    movement = false;
    Bean.sleep(200);
  }
}


// This function calculates the difference between two acceleration readings
int getAccelDifference(AccelerationReading readingOne, AccelerationReading readingTwo){
  int deltaX = abs(readingTwo.xAxis - readingOne.xAxis);
  int deltaY = abs(readingTwo.yAxis - readingOne.yAxis);
  int deltaZ = abs(readingTwo.zAxis - readingOne.zAxis);
  // Return the magnitude
  return deltaX + deltaY + deltaZ;   
}

// This Function Manages the alert System
void alertManager(){
  if(movement){
     //print the status message
    Serial.println("ALARM ACTIVATED!"); 
  
    //red light
    Bean.setLed(255, 0, 0);
    
    //tone to high freq.
    for(int i = 0; i < 5000; i+=25){
       tone(5, i);
       Bean.sleep(5);
    }
    
    //blue light
    Bean.setLed(0, 0, 255);
    
    //tone to low freq.
    for(int i = 5000; i > 0; i-=25){
       tone(5, i);
       Bean.sleep(5);
    }
    
    //stop the lights
    Bean.setLed(0, 0, 0);
   }  
}




