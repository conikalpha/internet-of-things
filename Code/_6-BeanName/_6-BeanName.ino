/* 
  This sketch shows you how to change the name of your Bean depending on the battery voltage.
  
  If the name doesn't change, try turning your device's Bluetooth off and on to clear the cache. 
  
  This example code is in the public domain.
*/

void setup() {
}

void loop()
{
 int bat = Bean.getBatteryLevel(); 
 String beanName = "Bean 1 ";
 beanName += bat; 
 beanName += "% Battery";
 
 Bean.setBeanName(beanName);
 
 // Sleep for a minute 
 Bean.sleep(86400000);
}
